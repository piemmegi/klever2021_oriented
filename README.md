# Simulation of the KLEVER 2021 beamtest data (implementing the physics of oriented crystals)

Code initially provided by: M. Soldani

Author and maintainer: [P. Monti-Guarnieri](mailto:pmontiguarnieri@studenti.uninsubria.it)

# Content

This code is used to simulate the KLEVER 2021 beamtest at the CERN SPS, complementing the work initiated in the [first simulation of the setup](https://gitlab.com/piemmegi/klever2021_simulation.git). The main difference with respect to the other code is the implementation of the **physics of oriented crystals**: this means that it is possible to simulate a number of physical quantities (e.g., the energy deposited in the sample under study) in both the random and axial alignment, and thus compare the results of the simulations with the experimental data. 

**More info about how this simulation works are given in the [Documentation](Documentation/doc_simulation) folder**

# A short guide on how to install and use Geant4 on WSL/CERN VM

For more details please see the [Documentation](Documentation/doc_G4installation). Note that here the Geant4 release used is the usual 11.0.2; however, the simulation was originally developed within Geant4 11.1.1 and it works fine even there.

# Quick commands and memo

- **Run for grahics**: start XLaunch -> Multiple windows -> Start no client -> Uncheck "Native opengl" and check "Disable access control"; then
```bash
./klever21
/control/execute macros/run_forgraphics.mac
```

- **Run in a given mode, for physics** (assuming Geant4 ver. 11.0.2, and assuming you work in local (for VM see the comandi.txt file)):
```bash
cd CERN2022-build
source ~/Geant4.11.0.2/geant4-11.0.2-install/bin/geant4.sh
cmake -DGeant4_DIR=~/Geant4.11.0.2/geant4-11.0.2-install/lib/Geant4-11.0.2/ ../CERN2022 && make -j4
./klever21 macros/run_forphysics.mac
```

- **Multi-run (long statistics, both amorphous and axial)**:
```bash
cd CERN2022-build
../mastercall.sh
```