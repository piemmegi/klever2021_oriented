# A short guide on how to install Geant4.11 and ROOT on WSL2

## Install WSL2

### Installation

[This](https://winaero.com/update-from-wsl-to-wsl-2-in-windows-10/#:~:text=To%20update%20from%20WSL%20to%20WSL%202%20in%20Windows%2010%2C&text=Restart%20Windows%2010.,set%2Ddefault%2Dversion%202%20.) is the reference guide. Shortly:

1. If you already have a WSL, you can skip this step. Otherwise, enable *Windows Subsystem for Linux* and *Virtual Machine platform* and then restart the system. The enabling can be done in several ways:
	- Through the `Turn on or off Windows Features`
	- Through Control Panel, under `Programs and features` (check the last item)
	- Through Power Shell, running as admin. In this case, just run the following lines of code:

	```powershell
	dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart
	dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart
	```

2. Download and install the latest Linux kernel update package from [this](https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi) link, or following step n.5 of the guide linked above.

3. Set WSL 2 as your default version, running the following command from Power Shell, as administrator. Note: this is optional, but in this way we are sure that any new distro installed is configured as WSL 2. 
```powershell
wsl --set-default-version 2
```

4. Install a distro for WSL. Important note: it is **highly recommended** to install Ubuntu 20.04.5 LTS or 20.04.6 LTS. The exact version is not a problem, however it is crucial **not to install Ubuntu 21.04 or any subsequent release**. The reason for this is the fact that, since Ubuntu 21.04 onwards, several packages needed for the installation of Geant4.11 have been dismissed. There are workarounds for this problem (e.g., see [here](https://askubuntu.com/questions/1335184/qt5-default-not-in-ubuntu-21-04)), but the easiest solution is probably to just install Ubuntu 20.04.5 LTS.

4. Check if the WSL is correctly set as WSL2 with
```powershell
wsl -l -v
wsl --set-version Ubuntu 2
```

	If it is not, get the name of the distro and set it to 2:
	```powershell
	wsl --set-version Ubuntu 2
	```
	where `Ubuntu` is the name of the distro.

### Allowing WSL2 to output graphics

1. First of all, VcXsrv in necessary. It can be downloaded [here](https://sourceforge.net/projects/vcxsrv/)
2. Add to `~/.bashrc` the following lines (as explained [here](https://stackoverflow.com/questions/61110603/how-to-set-up-working-x11-forwarding-on-wsl2)):
```
export DISPLAY=$(awk '/nameserver / {print $2; exit}' /etc/resolv.conf 2>/dev/null):0
export LIBGL_ALWAYS_INDIRECT=1
```

3. When you want to produce graphical output, you have to launch `XLaunch`, and then select:
	- Multiple windows
	- Start no client 
	- Uncheck "Native opengl"
	- Check "Disable access control"

### Backup commands

These commands are not needed, but it is useful to have them written somewhere...
```powershell
systeminfo | find "System Type"
wsl.exe --list --all
wsl --set-default-version 2
wsl --set-version Ubuntu 2
wsl -l -v
wsl --update
wsl --shutdown
```

## Install CMake, Make and C/C++ compilers
Now let's install CMake, Make and also a C and a C++ compiler (i.e., gcc and g++). **Pay attention to the outputs of each command**, since Linux *will* tell you if there is any problem to be solved (and usually it will also tell you *how to solve it*). 

```bash
sudo apt purge cmake
sudo apt install libssl-dev

mkdir CMake && cd CMake
wget https://github.com/Kitware/CMake/releases/download/v3.18.2/cmake-3.18.2.tar.gz
sudo tar xzf cmake-3.18.2.tar.gz && cd cmake-3.18.2

sudo apt-get update
sudo apt install gcc
sudo apt install g++
sudo apt-get install build-essential

sudo ./bootstrap
sudo make -j4
sudo make install
```
The boostrap and make phase should be the longest, while the install phase should be very quick.

## Install some more dependencies

```bash
sudo apt -y install libglu1-mesa-dev freeglut3-dev mesa-common-dev xorg
sudo apt -y install qtcreator qt5-default
sudo add-apt-repository ppa:rock-core/qt4
sudo apt update
sudo apt install libqt4-declarative qt4-dev-tools qt4-qmake libqtwebkit4
sudo apt -y install libxmu-dev libxerces-c-dev libexpat1-dev libssl-dev
sudo apt -y update && sudo apt -y upgrade
```

## Installation of Geant4 11.0.2
This version is the one within which the simulation was developed and tested, so it is the one recommended. It is generally compatible with any simulation produced by the Ferrara group (reference people: A. Sytov, G. Paternò, M. Soldani) and the Insulab group (reference people: P. Monti-Guarnieri, S. Carsi).

1. Create a folder and enter it
```bash
mkdir Geant4.11.0.2 && cd Geant4.11.0.2
```

2. Download the desired version (format .tar.gz) and extract it. You can find a complete list of the released versions [here](https://github.com/Geant4/geant4/releases), but currently we advice to choose 11.0.2.
```bash
wget https://github.com/Geant4/geant4/archive/refs/tags/v11.0.2.tar.gz && tar -xvf v11.0.2.tar.gz
```

3. Create a folder for the build and enter it
```bash
mkdir geant4-11.0.2-build && cd geant4-11.0.2-build
```

4. Run the following commands to build Geant4.
```bash
cmake -DCMAKE_INSTALL_PREFIX=~/Geant4.11.0.2/geant4-11.0.2-install ~/Geant4.11.0.2/geant4-11.0.2  -DGEANT4_USE_OPENGL_X11=ON -DGEANT4_USE_QT=ON -DGEANT_BUILD_MULTITHREADED=ON -DGEANT4_INSTALL_DATA=ON
make -j4
make install
```

Some caveats: `-j4` means that 4 processes are used for the build, which may be too much for the PC if you're running other programs in the meantime. If the process slows down your PC by a lot, try `-j2` or even `-j1` (it will take longer, but it will do its work). 

Moreover, pay attention to **where** you are building Geant4. If you're **not** located in the WSL 2 standard home directory (and subfolders), i.e. you are in `/mnt/c/something` instead of `/home/username/something`, the building process may slow down and stop at around 29% (at the `Scanning dependencies of target G4processes` step) for many hours. This happens due to how WSL works and how the I/O is defined with the `/mnt` folders. Thus, it is highly recommended to build Geant in the `/home/username/` folder.

5. For setting up all the variables, a `source` should be performed every time you start up your machine. It is convenient to add the following lines in `~/.bashrc` file, a script which is executed each time you login (*the path should be modified depending on where you are installing everything; keep in mind that `~` is the home folder)
```bash
export G4INSTALL=~/Geant4.11.0.2/geant4-11.0.2-install
source $G4INSTALL/bin/geant4.sh
```

## Install ROOT

1. Install the ROOT **dependencies**, which are listed in [this](https://root.cern/install/dependencies/#required-packages) page. Supposing to be in Ubuntu 20.04.5 LTS, the dependencies should be as follows:
```bash
sudo apt-get install dpkg-dev cmake g++ gcc binutils libx11-dev libxpm-dev libxft-dev libxext-dev python libssl-dev
sudo apt-get install gfortran libpcre3-dev xlibmesa-glu-dev libglew1.5-dev libftgl-dev libmysqlclient-dev libfftw3-dev libcfitsio-dev graphviz-dev libavahi-compat-libdnssd-dev libldap2-dev python-dev libxml2-dev libkrb5-dev libgsl0-dev qtwebengine5-dev
```

2. Download the ROOT binary files (already compiled) and then unpack them. 
```bash
wget https://root.cern/download/root_v6.26.06.Linux-ubuntu20-x86_64-gcc9.4.tar.gz
tar -xvzf root_v6.26.06.Linux-ubuntu20-x86_64-gcc9.4.tar.gz
```

3. Source the program at launch. In other words, add the following line to `~/.bashrc`:
```bash
source ~/root/bin/thisroot.sh
```

If you followed all the steps until this point, you should already be able to produce graphical outputs with ROOT (by using again XLaunch).

# A short guide on how to install Geant4.11 and ROOT on CERN VMs

There is no need to actually install the C++ compiler, CMake, Geant4 and ROOT. In facts, it is possible to directly source these programs through the following commands:

```bash
source /cvmfs/sft.cern.ch/lcg/contrib/gcc/9/x86_64-centos8/setup.sh
source /cvmfs/geant4.cern.ch/geant4/11.0.p02/x86_64-centos8-gcc9-optdeb-MT/GNUMake-setup.sh
source /cvmfs/sft.cern.ch/lcg/contrib/CMake/3.18.3/Linux-x86_64/setup.sh
source /cvmfs/sft.cern.ch/lcg/app/releases/ROOT/6.28.00/x86_64-centos8-gcc85-opt/bin/thisroot.sh
```

# Acknowledgements

Thanks to S. Carsi for writing the first draft of this guides.