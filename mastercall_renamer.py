# ==========================================================================================
#                                          HEADER
# ==========================================================================================
# The aim of this script is to move the files produces as output of the g4oreo simulation
# from the native folder to a safer location. 
# Note: NEVER CALL THIS SCRIPT ALONE! It is called inside the mastercall.sh script, so check
# that out instead of this.
#
# ==========================================================================================
#                            IMPORT OF THE EXTERNAL PYTHON MODULES
# ==========================================================================================
import os
import sys
import shutil

# ==========================================================================================
#                                         INPUTS
# ==========================================================================================
# Define some paths
thisfolder = os.path.dirname(os.path.abspath(__file__))            # Cartella della simulazione
outfolder = thisfolder + "/Outputfiles/"
G4outputname = 'outfile.root'
currname = thisfolder + '/CERN2022-build/out_data/' + G4outputname # File di output prodotto by default da G4
newoutname = outfolder + "tbeamdata_" + sys.argv[1] + ".root"      # File rinominato
copyname = outfolder + G4outputname

# ==========================================================================================
#                               RINOMINAZIONE DEL FILE
# ==========================================================================================
# Once the simulation has ended, move away the output file and rename it
shutil.copy(currname,outfolder)
os.rename(currname,newoutname)
os.remove(copyname)

# Close the script
print(f"Python task completed! \n")