﻿// -------------------------------------------------------------------
//
// GEANT4 Class file
//
// This file implements the class for the modified pair production process.
// Class based on G4PairProduction.
//
// -------------------------------------------------------------------
// Some include...

#include "GammaConversionCrystal.hh"
#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"
#include "G4BetheHeitlerModel.hh"
#include "G4PairProductionRelModel.hh"
#include "G4Electron.hh"
#include "G4EmParameters.hh"

#include "BetheHeitlerModel.hh"
#include "PairProductionRelModel.hh"
#include "G4BetheHeitlerModel.hh"
#include "G4PairProductionRelModel.hh"

// This is a later addition and is important to allow the localized modification
// of the G4 processes (in specified regions)

#include "G4RegionStore.hh"
#include "G4EmConfigurator.hh"
#include "G4LossTableManager.hh"

using namespace std;

GammaConversionCrystal::GammaConversionCrystal(const G4String& processName,
  G4ProcessType type):G4VEmProcess (processName, type),
    isInitialised(false)
{
  SetMinKinEnergy(2.0*electron_mass_c2);
  SetProcessSubType(fGammaGeneralProcess);
  SetStartFromNullFlag(true);
  SetBuildTableFlag(true);
  SetSecondaryParticle(G4Electron::Electron());
  SetLambdaBinning(220);
}
 
GammaConversionCrystal::~GammaConversionCrystal()
{}

G4bool GammaConversionCrystal::IsApplicable(const G4ParticleDefinition& p)
{
  return (&p == G4Gamma::Gamma());
}

void GammaConversionCrystal::InitialiseProcess(const G4ParticleDefinition*)
{
  if(!isInitialised) {
    // Instantiate the parameters for the pair production model
    G4EmParameters* param = G4EmParameters::Instance();
    G4double emin = std::max(param->MinKinEnergy(), 2*electron_mass_c2);
    G4double emax = param->MaxKinEnergy();
    SetMinKinEnergy(emin);

    // Activate the model for the world: G4 standard process (low-energy)
    if (!EmModel(0)) { SetEmModel(new G4BetheHeitlerModel()); }
    // if (!EmModel(0)) { SetEmModel(new BetheHeitlerModel()); }
    //                // Uncomment the previous line to go back to the SF process

    EmModel(0)->SetLowEnergyLimit(emin);
    G4double energyLimit = std::min(EmModel(0)->HighEnergyLimit(), 80*GeV);
    EmModel(0)->SetHighEnergyLimit(energyLimit);
    AddEmModel(1,EmModel(0));

    // Activate the model for the world: G4 standard process (high-energy)
    if(emax > energyLimit) {
      // Define model
      if (!EmModel(1)) { SetEmModel(new G4PairProductionRelModel()); }
      // if (!EmModel(1)) { SetEmModel(new PairProductionRelModel()); }
      //                // Uncomment the previous line to go back to the SF process
      
      // Set parameters and activate
      EmModel(1)->SetLowEnergyLimit(energyLimit);
      EmModel(1)->SetHighEnergyLimit(emax);
      AddEmModel(1,EmModel(1));
    }
 
    // Flag for initialization of the process
    isInitialised = true;
  } 
}

// Some other functions...

G4double GammaConversionCrystal::MinPrimaryEnergy(const G4ParticleDefinition*,
					     const G4Material*)
{
  return 2*electron_mass_c2;
}


//void GammaConversionCrystal::PrintInfo()
//{}         

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GammaConversionCrystal::ProcessDescription(std::ostream& out) const
{
  out << "  Gamma conversion for crystal";
  G4VEmProcess::ProcessDescription(out);
}

// From here onwards: old copies of the code. Do not uncomment!.
//
//
//G4double GammaConversionCrystal::PostStepGetPhysicalInteractionLength(
//                             const G4Track& track,
//                             G4double   previousStepSize,
//                             G4ForceCondition* condition)
//{
//    *condition = NotForced;
//    G4double x = DBL_MAX;
//    G4LogicalVolume* aLV = track.GetVolume()->GetLogicalVolume();
//    G4LogicalVolume* aNLV = track.GetNextVolume()->GetLogicalVolume();

//    if(LogicalCrystalVolume::IsLattice(aLV) == true &&
//            LogicalCrystalVolume::IsLattice(aNLV) == true){
//        return G4VEmProcess::PostStepGetPhysicalInteractionLength(track,previousStepSize,condition);
//    }
//    else{
//        x= DBL_MAX;
//        theNumberOfInteractionLengthLeft = -1.0;
//        currentInteractionLength = DBL_MAX;

//    }
//    return x;
//}

//G4VParticleChange* GammaConversionCrystal::PostStepDoIt(const G4Track& track,
//                                              const G4Step& step)
//{
//    G4LogicalVolume* aLV = track.GetVolume()->GetLogicalVolume();
//    G4LogicalVolume* aNLV = track.GetNextVolume()->GetLogicalVolume();

//    if(LogicalCrystalVolume::IsLattice(aLV) == true &&
//            LogicalCrystalVolume::IsLattice(aNLV) == true){
//        return G4VEmProcess::PostStepDoIt(track,step);
//    }
//    else
//    {
//        theNumberOfInteractionLengthLeft = -1.0;
//        mfpKinEnergy = DBL_MAX;
//        fParticleChange.InitializeForPostStep(track);
//        return &fParticleChange;
//    }
//}