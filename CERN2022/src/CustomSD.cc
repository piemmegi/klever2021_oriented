#include <G4SDManager.hh>
#include "CustomSD.hh"
#include "G4VProcess.hh"

// ============================================================================
//                                 CALORIMETRY
// ============================================================================

// VolumeEDepHit/SD, i.e. to detect the energy deposited inside a certain volume

VolumeEDepSD::VolumeEDepSD(G4String name) :  G4VSensitiveDetector(name)
{collectionName.insert("VolumeEDep");}

G4bool VolumeEDepSD::ProcessHits(G4Step* aStep, G4TouchableHistory*)
{
    // During each step where the SD is passed through by a particle,
    // record the total energy deposited in the active volume.
    VolumeEDepHit* hit = new VolumeEDepHit();
    hit->SetEDep(aStep->GetTotalEnergyDeposit());  // Get total energy deposited in the volume in the step
    fVolumeEDepHitsCollection->insert(hit);        // Save hit for readout
    return true;
}

void VolumeEDepSD::Initialize(G4HCofThisEvent* hcof)
{
    // Pre-initialize the SD at the BeginofEventAction level
    fVolumeEDepHitsCollection = new VolumeEDepHitsCollection(SensitiveDetectorName, collectionName[0]);
    if (fVolumeEDepHitsCollectionId < 0)
    {fVolumeEDepHitsCollectionId = G4SDManager::GetSDMpointer()->GetCollectionID(GetName() + "/" + collectionName[0]);}
    hcof->AddHitsCollection(fVolumeEDepHitsCollectionId, fVolumeEDepHitsCollection);
}

// ============================================================================
//                                 TRACKING
// ============================================================================

// VolumeTrackingHit/SD, i.e. to detect hit position and energy deposited for each track inside a certain volume

VolumeTrackingSD::VolumeTrackingSD(G4String name) :  G4VSensitiveDetector(name)
{collectionName.insert("VolumeTracking");}

G4bool VolumeTrackingSD::ProcessHits(G4Step* aStep, G4TouchableHistory*)
{
    // During each step where the SD is passed through by a particle,
    // record the total energy deposited in the active volume.
    // Moreover, register the hit position and the track identifier
    // (which can be later retrieved for other analyses)
    VolumeTrackingHit* hit = new VolumeTrackingHit();
    hit->SetTrackId(aStep->GetTrack()->GetTrackID());    // getting current track ID
    hit->SetX(aStep->GetPreStepPoint()->GetPosition());  // getting position at the beginning of the step
    hit->SetEDep(aStep->GetTotalEnergyDeposit());        // getting total energy deposited in the volume in the step 	

    // Additionally, we can save the particle total energy, if it is the primary
    if (aStep->GetTrack()->GetCreatorProcess()) {
        // Here we enter if the particle is secondary, i.e., if it is CREATED

        /*
        G4int thisTrackID = aStep->GetTrack()->GetTrackID();
        G4String CPname = aStep->GetTrack()->GetCreatorProcess()->GetProcessName();
        if ((thisTrackID != 1) && (CPname == "eBrem")) {
            // Here we enter if the particle is a gamma produced by BS: we score the energy
            hit->SetPPTE(aStep->GetTrack()->GetTotalEnergy());
        } else {
            // Here we enter if the particle is secondary but not a BS gamma: score a null value
            hit->SetPPTE(-1);
        }
        */
        hit->SetPPTE(-1);
    } else {
        // Here we enter if the particle is the primary. in this case, score a null value.
        hit->SetPPTE(aStep->GetTrack()->GetTotalEnergy());
    }

    fVolumeTrackingHitsCollection->insert(hit);
    return true;
}

void VolumeTrackingSD::Initialize(G4HCofThisEvent* hcof)
{
    // Pre-initialize the SD at the BeginofEventAction level
    fVolumeTrackingHitsCollection = new VolumeTrackingHitsCollection(SensitiveDetectorName, collectionName[0]);
    if (fVolumeTrackingHitsCollectionId < 0)
    {fVolumeTrackingHitsCollectionId = G4SDManager::GetSDMpointer()->GetCollectionID(GetName() + "/" + collectionName[0]);}
    hcof->AddHitsCollection(fVolumeTrackingHitsCollectionId, fVolumeTrackingHitsCollection);
}