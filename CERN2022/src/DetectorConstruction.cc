// Standard includes
#include <G4SystemOfUnits.hh>
#include <G4LogicalVolume.hh>
#include <G4PVPlacement.hh>
#include <G4NistManager.hh>
#include <G4SystemOfUnits.hh>
#include <G4VisAttributes.hh>
#include <G4SDManager.hh>
#include <G4LogicalVolumeStore.hh>
#include <G4UniformMagField.hh>
#include <G4FieldManager.hh>
#include <G4TransportationManager.hh>
#include <G4ChordFinder.hh>
#include <G4MultiFunctionalDetector.hh>
#include <G4Box.hh>

#include "DetectorConstruction.hh"
#include "CustomSD.hh"

#include <G4Tubs.hh>
#include <G4SubtractionSolid.hh>

#include "LogicalCrystalVolume.hh"  // added to exploit modified Geant4

#include "G4ProductionCuts.hh"
#include "G4RegionStore.hh"

DetectorConstruction::DetectorConstruction()
:G4VUserDetectorConstruction(),
 fpRegion(0)
{}  

// DetectorConstruction::Construct, i.e. where the setup geometry is implemented
G4VPhysicalVolume* DetectorConstruction::Construct()
{
    // ============================================================================
    //                                 MATERIALS DEFINITION
    // ============================================================================
    // Retrieve the NIST database for materials
    G4NistManager* nist = G4NistManager::Instance();
    
    // Define if the crystal under test must be placed or not
    // (Note to self (2023.03.03): if not -> useful for calibration runs, 
    // I suppose? Unclear in the original code...)
    G4bool bCrys = true; // crystal placement switch
	
    // Create the colors used in the visualization
    G4VisAttributes* grey = new G4VisAttributes(true, G4Colour::Grey());
    G4VisAttributes* cyan = new G4VisAttributes(true, G4Colour::Cyan());
	G4VisAttributes* green = new G4VisAttributes(true, G4Colour::Green());
	G4VisAttributes* white = new G4VisAttributes(true, G4Colour::White());
	//G4VisAttributes* invisible = new G4VisAttributes(false);
	G4VisAttributes* brown = new G4VisAttributes(true, G4Colour::Brown());
    G4VisAttributes* blue = new G4VisAttributes(true, G4Colour::Blue());

    // Create the most important materials
    G4Material* air = nist->FindOrBuildMaterial("G4_AIR");      // air
    G4Material* silicon = nist->FindOrBuildMaterial("G4_Si");   // silicon
    G4Material* aluminum = nist->FindOrBuildMaterial("G4_Al");  // aluminum
    G4Material* iron = nist->FindOrBuildMaterial("G4_Fe");      // iron 
    G4Material* pwo = nist->FindOrBuildMaterial("G4_PbWO4");    // lead tungstate
    G4Material* bgo = nist->FindOrBuildMaterial("G4_BGO");      // BGO
    //G4Material* ss = nist->FindOrBuildMaterial("G4_STAINLESS-STEEL");  // stainless steel, for boxes
	//G4Material* vacuum = nist->FindOrBuildMaterial("G4_Galactic");     // vacuum
	//G4Material* lead = nist->FindOrBuildMaterial("G4_Pb");      // lead
	G4Material* plastic = nist->FindOrBuildMaterial("G4_POLYSTYRENE"); // polystirene, for scintillators 
    G4Material* copper= nist->FindOrBuildMaterial("G4_Cu");     // copper

    // Build manually the PbF2, since it does not exist in the NIST repository
    G4int ncomponents;
    G4int natoms;
    G4Element* Pb = nist->FindOrBuildElement(82);
    G4Element* F = nist->FindOrBuildElement(9);
    G4double density = 7.770 *g/cm3;

    G4Material* PbF2 = new G4Material("PbF2", density, ncomponents= 2);
    
    PbF2->AddElement(Pb, natoms=1);
    PbF2->AddElement(F, natoms=2);

    // world
    G4double worldSizeX = 10 * m;
    G4double worldSizeY = 10 * m;
    G4double worldSizeZ = 80 * m;
    G4VSolid* worldBox = new G4Box("World_Solid", worldSizeX / 2, worldSizeY / 2, worldSizeZ / 2);

    G4LogicalVolume* worldLog = new G4LogicalVolume(worldBox, air, "World_Logical");
    G4VisAttributes* visAttrWorld = new G4VisAttributes();
    visAttrWorld->SetVisibility(false);
    worldLog->SetVisAttributes(visAttrWorld);

    G4VPhysicalVolume* worldPhys = new G4PVPlacement(nullptr, {}, worldLog, "World", nullptr, false, 0);
	
    // ============================================================================
    //                                 DISTANCES
    // ============================================================================
    // Define the detectors positions along the beam axis (z)
    // (ALL THE DETECTORS)
    //G4double zTrackerSiliCentre0 = 0 * cm;     // Tele1 (module 0) longitudinal centre (4.5 cm long)
    G4double zTrackerSiliCentre0 = 10 * cm;     // Tele1 (module 0) longitudinal centre (4.5 cm long)
    G4double zTrackerSiliCentre1 = 13.65 * m;  // Tele2 (module 1) longitudinal centre (4.5 cm long)
    G4double zTrackerSiliCentre2 = 18.90 * m;  // Chamber1 (module 2) longitudinal centre (4.8 cm long)
    G4double zTrackerSiliCentre3 = 32.61 * m;  // Chamber2 (module 3) longitudinal centre (4.8 cm long)
    G4double zMagnet = zTrackerSiliCentre2 + 20 * cm + 1 * m;  // bending magnet longitudinal centre
    G4double zTgt = 28.67 * m;          // Crystal under test (target) longitudinal centre
    G4double zPhCaloFront = 32.98 * m;  // Genni/gammaCAL (BGO) longitudinal front
    G4double zOutCounter = 32.46 * m;   // Multiplicity counter longitudinal centre
	G4double zBS_Target = 14.43 * m;    // Bremsstrahlung target (copper)
    G4double zTagger = zBS_Target + 0.10 * m;

    // ============================================================================
    //                                 CRYSTAL TARGET
    // ============================================================================
    // Define the dimensions of the crystal target (consider the samples used in KLEVER2021)
    G4bool ifPbF2 = false;  // "true" -> PbF2; "false" -> PbWO4
    G4double tgtThickness;  // z
    G4double tgtWidth;      // x
    G4double tgtHeight;     // y

    if (ifPbF2) {
        // Assume the crystal is made of PbF2
        tgtThickness = 2 * PbF2->GetRadlen();  // z
        tgtWidth = 1 * PbF2->GetRadlen();      // x
        tgtHeight = 2 * PbF2->GetRadlen();     // y
    }
    else {
        // Assume the crystal is made of PbWO4
        tgtThickness = 1 * 0.8903 * cm;  // z
        tgtWidth = 3 * 0.8903 * cm;      // x
        tgtHeight = 3 * 0.8903 * cm;     // y
    }

    // Construct the physical volume    
    G4VSolid* tgtBox = new G4Box("Tgt_Solid", tgtWidth / 2, tgtHeight / 2, tgtThickness / 2);
	
    // Construct the logical volume, considering if we are running in "axial" or "amorphous" mode
    if(bChanneling){
        // "Axial" mode. Note: use custom class for the logical volume placement.
        LogicalCrystalVolume* tgtLog_channeling;

        if (ifPbF2){
            tgtLog_channeling = new LogicalCrystalVolume(tgtBox, PbF2, "Tgt_Logical");
        } else {
            tgtLog_channeling = new LogicalCrystalVolume(tgtBox, pwo, "Tgt_Logical");
        }
	    
        tgtLog_channeling->SetVisAttributes(blue);
		
		if(bCrys){
            // Place the crystal physical volume
            new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zTgt), tgtLog_channeling, "Tgt", worldLog, false, 0);
            
            // Add the crystal target region (used for localized application of SF interactions)
            fpRegion2 = new G4Region("SF_Region");
            G4ProductionCuts* cuts = new G4ProductionCuts();
            G4double defCut = 1*mm;
            cuts->SetProductionCut(defCut,"gamma");
            cuts->SetProductionCut(defCut,"e-");
            cuts->SetProductionCut(defCut,"e+");
            cuts->SetProductionCut(defCut,"proton");
            fpRegion2->SetProductionCuts(cuts);
            fpRegion2->AddRootLogicalVolume(tgtLog_channeling);
            
        };
		
	} else {
        G4LogicalVolume* tgtLog;

        if (ifPbF2){
            tgtLog = new G4LogicalVolume(tgtBox, PbF2, "Tgt_Logical");
        } else {
            tgtLog = new G4LogicalVolume(tgtBox, pwo, "Tgt_Logical");
        }

	    tgtLog->SetVisAttributes(blue);
		
		if(bCrys){
            // Place the crystal physical volume
            new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zTgt), tgtLog, "Tgt", worldLog, false, 0);

            // Note that here there is no necessity to define a G4Region, since the PhysicsList is everywhere the same...
        };
	}

    // ============================================================================
    //                                 OTHER VOLUMES
    // ============================================================================
    // Place the copper target
    G4Box* BS_Target_box = new G4Box("BStarget_physical",10*cm/2, 10*cm/2, 1*mm/2);
    G4LogicalVolume* fLogicRame = new G4LogicalVolume(BS_Target_box, copper,"BStarget_logical");
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zBS_Target), fLogicRame, "BStarget", worldLog, false, 0);

    // Place the tagger
    G4Box* tagger_box = new G4Box("Tagger_physical",100*cm/2, 100*cm/2, 10*mm/2);
    G4LogicalVolume* fLogicTagger = new G4LogicalVolume(tagger_box, air,"Tagger_logical");
    fLogicTagger->SetVisAttributes(brown);
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zTagger), fLogicTagger, "Tagger_placed", worldLog, false, 0);

    // Place the bending magnet
    G4double magnetYokeThickness = 2 * m;
    G4double magnetYokeWidth = 1.75 * m;
    G4double magnetYokeHeight = 1.12 * m;
    G4double magnetFieldWidth = 1.12 * m;
	G4double magnetFieldHeight = 110 * mm;
    G4VSolid* magnetYokeBox_0 = new G4Box("magnetYokeOuter_Solid", magnetYokeWidth / 2, magnetYokeHeight / 2, magnetYokeThickness / 2);
	G4VSolid* magnetYokeBox_I = new G4Box("magnetYokeInner_Solid", magnetFieldWidth / 2, magnetFieldHeight / 2, magnetYokeThickness / 2 + 0.1*mm);
	G4SubtractionSolid* magnetYokeBox = new G4SubtractionSolid("magnetYoke_Solid", magnetYokeBox_0, magnetYokeBox_I);
    G4VSolid* magnetFieldBox = new G4Box("magnetField_Solid", magnetFieldWidth / 2, magnetFieldHeight / 2, magnetYokeThickness / 2);
	
    G4LogicalVolume* magnetYokeLog = new G4LogicalVolume(magnetYokeBox, iron, "magnetYoke_Logical");
    magnetYokeLog->SetVisAttributes(green);
    G4LogicalVolume* magnetFieldLog = new G4LogicalVolume(magnetFieldBox, air, "magnetField_Logical");
    G4VisAttributes* visAttrField = new G4VisAttributes();
    visAttrField->SetVisibility(false);
    magnetFieldLog->SetVisAttributes(visAttrField);
	
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zMagnet), magnetYokeLog, "magnetYoke", worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zMagnet), magnetFieldLog, "magnetField", worldLog, false, 0);
	
    // Place the silicon trackers
    G4double trackerSiliThickness = 300 * um;     // Telescopes
    G4double trackerSiliWidth = 384 * 50 * um;
    G4double trackerSiliHeight = 384 * 50 * um;
    G4VSolid* trackerSiliBox = new G4Box("TrackerSili_Solid", trackerSiliWidth / 2, trackerSiliHeight / 2, trackerSiliThickness / 2);
    G4double trackerSiliBigThickness = 410 * um;  // Chambers
    G4double trackerSiliBigWidth = 384 * 242 * um;
    G4double trackerSiliBigHeight = 384 * 242 * um;
    G4VSolid* trackerSiliBigBox = new G4Box("TrackerSiliBig_Solid", trackerSiliBigWidth / 2, trackerSiliBigHeight / 2, trackerSiliBigThickness / 2);
    G4double trackerAlThickness = 10 * um;
    G4double trackerAlWidth = 2 * cm;
    G4double trackerAlBigHeight = 10 * cm;
    G4double trackerAlBigWidth = 10 * cm;
    G4double trackerAlHeight = 2 * cm;
    G4VSolid* trackerAlBox = new G4Box("TrackerAl_Solid", trackerAlWidth / 2, trackerAlHeight / 2, trackerAlThickness / 2);
    G4VSolid* trackerAlBigBox = new G4Box("TrackerAlBig_Solid", trackerAlBigWidth / 2, trackerAlBigHeight / 2, trackerAlThickness / 2);
	G4SubtractionSolid* trackerAlSideFrontBox = new G4SubtractionSolid("TrackerAlBoxSideFront_Solid", new G4Box("", 20 * cm, 20 * cm, 1 * mm), new G4Box("", trackerAlWidth / 2, trackerAlHeight / 2, 1.1*mm));
	G4SubtractionSolid* trackerAlSideFrontBigBox = new G4SubtractionSolid("TrackerAlBoxSideFrontBig_Solid", new G4Box("", 20 * cm, 20 * cm, 1 * mm), new G4Box("", trackerAlBigWidth / 2, trackerAlBigHeight / 2, 1.1*mm));
	G4VSolid* trackerAlSideBackBox = new G4Box("TrackerAlBoxSideBack_Solid", 20 * cm, 20 * cm, 1 * mm);
	
    G4LogicalVolume* trackerSiliLog0 = new G4LogicalVolume(trackerSiliBox, silicon, "TrackerSili_Logical_0");  // tracker 0 is small
    G4LogicalVolume* trackerSiliLog1 = new G4LogicalVolume(trackerSiliBox, silicon, "TrackerSili_Logical_1");  // tracker 1 is small
    G4LogicalVolume* trackerSiliLog2_0 = new G4LogicalVolume(trackerSiliBigBox, silicon, "TrackerSili_Logical_2_0");  // tracker 2 is big & double -- 0th
    G4LogicalVolume* trackerSiliLog2_1 = new G4LogicalVolume(trackerSiliBigBox, silicon, "TrackerSili_Logical_2_1");  // tracker 2 is big & double -- 1st
    G4LogicalVolume* trackerSiliLog3_0 = new G4LogicalVolume(trackerSiliBigBox, silicon, "TrackerSili_Logical_3_0");  // tracker 3 is big & double -- 0th
    G4LogicalVolume* trackerSiliLog3_1 = new G4LogicalVolume(trackerSiliBigBox, silicon, "TrackerSili_Logical_3_1");  // tracker 3 is big & double -- 1st
    G4LogicalVolume* trackerAlLog = new G4LogicalVolume(trackerAlBox, aluminum, "TrackerAl_Logical");
    G4LogicalVolume* trackerAlBigLog = new G4LogicalVolume(trackerAlBigBox, aluminum, "TrackerAlBig_Logical");
    G4LogicalVolume* trackerAlSideFrontLog = new G4LogicalVolume(trackerAlSideFrontBox, aluminum, "TrackerAlSideFrontBox_Logical");
	G4LogicalVolume* trackerAlSideFrontBigLog = new G4LogicalVolume(trackerAlSideFrontBigBox, aluminum, "TrackerAlSideFrontBigBox_Logical");
	G4LogicalVolume* trackerAlSideBackLog = new G4LogicalVolume(trackerAlSideBackBox, aluminum, "TrackerAlBoxSideBack_Logical");
    trackerSiliLog0->SetVisAttributes(grey);
    trackerSiliLog1->SetVisAttributes(grey);
    trackerSiliLog2_0->SetVisAttributes(grey);
    trackerSiliLog2_1->SetVisAttributes(grey);
    trackerAlLog->SetVisAttributes(grey);
    trackerAlBigLog->SetVisAttributes(grey);
    trackerAlSideFrontLog->SetVisAttributes(grey);
    trackerAlSideFrontBigLog->SetVisAttributes(grey);
    trackerAlSideBackLog->SetVisAttributes(grey);
	
    G4double zTrackerAlFrontCentre0 = zTrackerSiliCentre0 - (2.25*cm - trackerAlThickness / 2);
    G4double zTrackerAlBackCentre0 = zTrackerSiliCentre0 + (2.25*cm - trackerAlThickness / 2);
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zTrackerSiliCentre0), trackerSiliLog0, "TrackerSili_0", worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zTrackerAlFrontCentre0), trackerAlLog, "TrackerSili_0_Al_Front", worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zTrackerAlBackCentre0), trackerAlLog, "TrackerSili_0_Al_Rear", worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zTrackerAlFrontCentre0), trackerAlSideFrontLog, "TrackerSili_0_Al_Front", worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zTrackerAlBackCentre0), trackerAlSideBackLog, "TrackerSili_0_Al_Rear", worldLog, false, 0);
    G4double zTrackerAlFrontCentre1 = zTrackerSiliCentre1 - (2.25*cm - trackerAlThickness / 2);
    G4double zTrackerAlBackCentre1 = zTrackerSiliCentre1 + (2.25*cm - trackerAlThickness / 2);
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zTrackerSiliCentre1), trackerSiliLog1, "TrackerSili_1", worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zTrackerAlFrontCentre1), trackerAlLog, "TrackerSili_1_Al_Front", worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zTrackerAlBackCentre1), trackerAlLog, "TrackerSili_1_Al_Rear", worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zTrackerAlFrontCentre1), trackerAlSideFrontLog, "TrackerSili_1_Al_Front", worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zTrackerAlBackCentre1), trackerAlSideBackLog, "TrackerSili_1_Al_Rear", worldLog, false, 0);
    G4double zTrackerAlFrontCentre2 = zTrackerSiliCentre2 - (2.4*cm - trackerAlThickness / 2);
    G4double zTrackerAlBackCentre2 = zTrackerSiliCentre2 + (2.4*cm - trackerAlThickness / 2);
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zTrackerSiliCentre2-0.5*cm), trackerSiliLog2_0, "TrackerSili_2_0", worldLog, false, 0);  // tracker 2 sensors are separated by 1 cm
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zTrackerSiliCentre2+0.5*cm), trackerSiliLog2_1, "TrackerSili_2_1", worldLog, false, 0);  // tracker 2 sensors are separated by 1 cm
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zTrackerAlFrontCentre2), trackerAlBigLog, "TrackerSili_2_Al_Front", worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zTrackerAlBackCentre2), trackerAlBigLog, "TrackerSili_2_Al_Rear", worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zTrackerAlFrontCentre2), trackerAlSideFrontBigLog, "TrackerSili_2_Al_Front", worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zTrackerAlBackCentre2), trackerAlSideBackLog, "TrackerSili_2_Al_Rear", worldLog, false, 0);
    G4double zTrackerAlFrontCentre3 = zTrackerSiliCentre3 - (2.4*cm - trackerAlThickness / 2);
    G4double zTrackerAlBackCentre3 = zTrackerSiliCentre3 + (2.4*cm - trackerAlThickness / 2);
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zTrackerSiliCentre3-0.5*cm), trackerSiliLog3_0, "TrackerSili_3_0", worldLog, false, 0);  // tracker 3 sensors are separated by 1 cm
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zTrackerSiliCentre3+0.5*cm), trackerSiliLog3_1, "TrackerSili_3_1", worldLog, false, 0);  // tracker 3 sensors are separated by 1 cm
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zTrackerAlFrontCentre3), trackerAlBigLog, "TrackerSili_3_Al_Front", worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zTrackerAlBackCentre3), trackerAlBigLog, "TrackerSili_3_Al_Rear", worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zTrackerAlFrontCentre3), trackerAlSideFrontBigLog, "TrackerSili_3_Al_Front", worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zTrackerAlBackCentre3), trackerAlSideBackLog, "TrackerSili_3_Al_Rear", worldLog, false, 0);
	
    // Photon omogeneous (BGO) calorimeter
    G4double caloPhThickness = 23 * cm;        // Genni: about 20 X0
    G4double caloPhWidthChannel = 2.1 * cm;
    G4double caloPhHeightChannel = 2.1 * cm;
	G4double caloPhGap = 1 * mm;
	G4double caloPhTransvDelta = caloPhHeightChannel + caloPhGap;
    G4VSolid* caloPhChannelBox = new G4Box("PhCalTest_Solid", caloPhWidthChannel / 2, caloPhHeightChannel / 2, caloPhThickness / 2);
	
    G4LogicalVolume* caloPhLog_00 = new G4LogicalVolume(caloPhChannelBox, bgo, "PhCalTest_00_Logical");
    G4LogicalVolume* caloPhLog_01 = new G4LogicalVolume(caloPhChannelBox, bgo, "PhCalTest_01_Logical");
    G4LogicalVolume* caloPhLog_02 = new G4LogicalVolume(caloPhChannelBox, bgo, "PhCalTest_02_Logical");
    G4LogicalVolume* caloPhLog_10 = new G4LogicalVolume(caloPhChannelBox, bgo, "PhCalTest_10_Logical");
    G4LogicalVolume* caloPhLog_11 = new G4LogicalVolume(caloPhChannelBox, bgo, "PhCalTest_11_Logical"); // central channel
    G4LogicalVolume* caloPhLog_12 = new G4LogicalVolume(caloPhChannelBox, bgo, "PhCalTest_12_Logical");
    G4LogicalVolume* caloPhLog_20 = new G4LogicalVolume(caloPhChannelBox, bgo, "PhCalTest_20_Logical");
    G4LogicalVolume* caloPhLog_21 = new G4LogicalVolume(caloPhChannelBox, bgo, "PhCalTest_21_Logical");
    G4LogicalVolume* caloPhLog_22 = new G4LogicalVolume(caloPhChannelBox, bgo, "PhCalTest_22_Logical");
    caloPhLog_00->SetVisAttributes(white);
    caloPhLog_01->SetVisAttributes(white);
    caloPhLog_02->SetVisAttributes(white);
    caloPhLog_10->SetVisAttributes(white);
    caloPhLog_11->SetVisAttributes(white); // central channel
    caloPhLog_12->SetVisAttributes(white);
    caloPhLog_20->SetVisAttributes(white);
    caloPhLog_21->SetVisAttributes(white);
    caloPhLog_22->SetVisAttributes(white);
	
    new G4PVPlacement(nullptr, G4ThreeVector(-caloPhTransvDelta, -caloPhTransvDelta, zPhCaloFront + caloPhThickness / 2), caloPhLog_00, "PhCalTest_00", worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(0, -caloPhTransvDelta, zPhCaloFront + caloPhThickness / 2), caloPhLog_01, "PhCalTest_01", worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(caloPhTransvDelta, -caloPhTransvDelta, zPhCaloFront + caloPhThickness / 2), caloPhLog_02, "PhCalTest_02", worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(-caloPhTransvDelta, 0, zPhCaloFront + caloPhThickness / 2), caloPhLog_10, "PhCalTest_10", worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zPhCaloFront + caloPhThickness / 2), caloPhLog_11, "PhCalTest_11", worldLog, false, 0); // central channel
    new G4PVPlacement(nullptr, G4ThreeVector(caloPhTransvDelta, 0, zPhCaloFront + caloPhThickness / 2), caloPhLog_12, "PhCalTest_12", worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(-caloPhTransvDelta, caloPhTransvDelta, zPhCaloFront + caloPhThickness / 2), caloPhLog_20, "PhCalTest_20", worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(0, caloPhTransvDelta, zPhCaloFront + caloPhThickness / 2), caloPhLog_21, "PhCalTest_21", worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(caloPhTransvDelta, caloPhTransvDelta, zPhCaloFront + caloPhThickness / 2), caloPhLog_22, "PhCalTest_22", worldLog, false, 0);
    	
	// Multiplicity Counter scintillator
	G4double dxScintiCavoto = 10*cm;
	G4double dyScintiCavoto = 10*cm;
	G4double dzScintiCavoto = 4.5*cm;
	G4VSolid* scintiCavotoSolid = new G4Box("scintiCavoto_Solid", dxScintiCavoto / 2, dyScintiCavoto / 2, dzScintiCavoto / 2);
	
	G4LogicalVolume* scintiCavotoLog = new G4LogicalVolume(scintiCavotoSolid, plastic, "scintiCavoto_Logical");
    scintiCavotoLog->SetVisAttributes(cyan);
	new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zOutCounter), scintiCavotoLog, "scintiCavoto", worldLog, false, 0);
    
    // Backup multiplicity counter for testing (note: this is NOT in the setup. it is a scintillator
    // made of pwo and very thick, which should allow to correctly determine if all went good)
    /*
    //G4LogicalVolume* scintiCavotoLog = new G4LogicalVolume(scintiCavotoSolid, pwo, "scintiCavoto_Logical");
    G4LogicalVolume* scintiCavotoLog = new G4LogicalVolume(scintiCavotoSolid, plastic, "scintiCavoto_Logical");
    scintiCavotoLog->SetVisAttributes(cyan);
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zTgt-30*cm), scintiCavotoLog, "scintiCavoto", worldLog, false, 0);
    */
    // Note to self: we are still missing the other calorimeters...

	// ============================================================================
    //                                 CONCLUDE
    // ============================================================================
    // Print list of defined material
    G4cout << "-----" << G4endl;
    G4cout << "| DetectorConstruction.cc: material list" << G4endl;
    G4cout << *(G4Material::GetMaterialTable()) << G4endl;
    G4cout << "-----" << G4endl;
	
    return worldPhys;
}

// DetectorConstruction::ConstructSDandField, i.e. where the sensitive detectors and magnetic fields are implemented
void DetectorConstruction::ConstructSDandField()
{
    // ============================================================================
    //                                 MAGNETIC FIELD
    // ============================================================================
    // Define the magnetic field strength
    G4double magnField = (4.05778/2) * tesla;
    G4UniformMagField* bendingField = new G4UniformMagField(G4ThreeVector(0., magnField, 0.));
    G4LogicalVolume* fieldLog = G4LogicalVolumeStore::GetInstance()->GetVolume("magnetField_Logical");
    G4FieldManager* fieldManager = new G4FieldManager(bendingField);
    fieldLog->SetFieldManager(fieldManager, true);
	
    // ============================================================================
    //                                 Sensitive Detectors
    // ============================================================================
    // Load the sensitive detector manager
    G4SDManager* sdm = G4SDManager::GetSDMpointer();
    sdm->SetVerboseLevel(1);  // set sensitive detector manager verbosity here

    // SD: gamma calorimeter
    VolumeEDepSD* gammaCalSD_00 = new VolumeEDepSD("PhCalTest_00_SD");
    SetSensitiveDetector("PhCalTest_00_Logical", gammaCalSD_00);
    sdm->AddNewDetector(gammaCalSD_00);
    VolumeEDepSD* gammaCalSD_01 = new VolumeEDepSD("PhCalTest_01_SD");
    SetSensitiveDetector("PhCalTest_01_Logical", gammaCalSD_01);
    sdm->AddNewDetector(gammaCalSD_01);
    VolumeEDepSD* gammaCalSD_02 = new VolumeEDepSD("PhCalTest_02_SD");
    SetSensitiveDetector("PhCalTest_02_Logical", gammaCalSD_02);
    sdm->AddNewDetector(gammaCalSD_02);
    VolumeEDepSD* gammaCalSD_10 = new VolumeEDepSD("PhCalTest_10_SD");
    SetSensitiveDetector("PhCalTest_10_Logical", gammaCalSD_10);
    sdm->AddNewDetector(gammaCalSD_10);
    VolumeEDepSD* gammaCalSD_11 = new VolumeEDepSD("PhCalTest_11_SD");
    SetSensitiveDetector("PhCalTest_11_Logical", gammaCalSD_11);
    sdm->AddNewDetector(gammaCalSD_11);
    VolumeEDepSD* gammaCalSD_12 = new VolumeEDepSD("PhCalTest_12_SD");
    SetSensitiveDetector("PhCalTest_12_Logical", gammaCalSD_12);
    sdm->AddNewDetector(gammaCalSD_12);
    VolumeEDepSD* gammaCalSD_20 = new VolumeEDepSD("PhCalTest_20_SD");
    SetSensitiveDetector("PhCalTest_20_Logical", gammaCalSD_20);
    sdm->AddNewDetector(gammaCalSD_20);
    VolumeEDepSD* gammaCalSD_21 = new VolumeEDepSD("PhCalTest_21_SD");
    SetSensitiveDetector("PhCalTest_21_Logical", gammaCalSD_21);
    sdm->AddNewDetector(gammaCalSD_21);
    VolumeEDepSD* gammaCalSD_22 = new VolumeEDepSD("PhCalTest_22_SD");
    SetSensitiveDetector("PhCalTest_22_Logical", gammaCalSD_22);
    sdm->AddNewDetector(gammaCalSD_22);
    
    // SD: crystal under test
    VolumeEDepSD* tgtSD_22 = new VolumeEDepSD("Tgt_SD");
    SetSensitiveDetector("Tgt_Logical", tgtSD_22);
    sdm->AddNewDetector(tgtSD_22);

    // SD: Multiplicity Counter
    VolumeEDepSD* MC_SD = new VolumeEDepSD("MC_SD");
    SetSensitiveDetector("scintiCavoto_Logical", MC_SD);
    sdm->AddNewDetector(MC_SD);
	
    // SD: Tagger
    VolumeTrackingSD* taggerSD = new VolumeTrackingSD("Tagger_SD");
    SetSensitiveDetector("Tagger_logical", taggerSD);
    sdm->AddNewDetector(taggerSD);

    // SD: silicon trackers (1 sensitive detector per tracking plane)
    // (meaning 1 SD for each telescope and 2 SD for each chamber)
    VolumeTrackingSD* trackerSD0 = new VolumeTrackingSD("Tracker_SD_0");
    SetSensitiveDetector("TrackerSili_Logical_0", trackerSD0);
    sdm->AddNewDetector(trackerSD0);
    VolumeTrackingSD* trackerSD1 = new VolumeTrackingSD("Tracker_SD_1");
    SetSensitiveDetector("TrackerSili_Logical_1", trackerSD1);
    sdm->AddNewDetector(trackerSD1);
    VolumeTrackingSD* trackerSD2_0 = new VolumeTrackingSD("Tracker_SD_2_0");
    SetSensitiveDetector("TrackerSili_Logical_2_0", trackerSD2_0);
    sdm->AddNewDetector(trackerSD2_0);
    VolumeTrackingSD* trackerSD2_1 = new VolumeTrackingSD("Tracker_SD_2_1");
    SetSensitiveDetector("TrackerSili_Logical_2_1", trackerSD2_1);
    sdm->AddNewDetector(trackerSD2_1);
    VolumeTrackingSD* trackerSD3_0 = new VolumeTrackingSD("Tracker_SD_3_0");
    SetSensitiveDetector("TrackerSili_Logical_3_0", trackerSD3_0);
    sdm->AddNewDetector(trackerSD3_0);
    VolumeTrackingSD* trackerSD3_1 = new VolumeTrackingSD("Tracker_SD_3_1");
    SetSensitiveDetector("TrackerSili_Logical_3_1", trackerSD3_1);
    sdm->AddNewDetector(trackerSD3_1);
}