﻿#ifndef EmStandardPhysics_h
#define EmStandardPhysics_h 1
 
#include "G4VPhysicsConstructor.hh"
#include "globals.hh"
#include "G4EmParticleList.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//
// Custom EmPhysicsList used to activate SF effects in crystals by properly
// modifing the cross-sections for pair-prodcution and Bremsstrahlung.
// Originally created by Viktar and then modified by Pietro Monti Guarnieri to
// activate SF effects in a region and then by gpaterno to activate SF effects
// in more than one region and pass a parameter to select the proper correction
// to apply depending on the material/axis considered.
// 
// Last modify: 05/04/2023
//
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class EmStandardPhysics : public G4VPhysicsConstructor
{
public:

  explicit EmStandardPhysics(G4int ver=0, 
                             const G4String& name="", 
                             const G4int matID=0);
 
  virtual ~EmStandardPhysics();

  virtual void ConstructParticle();

  virtual void ConstructProcess();
 
 private:
   G4int  verbose;
   G4EmParticleList partList;
   
   G4int fMatID; //gpaterno
 }; 
 //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
 
 #endif
 
